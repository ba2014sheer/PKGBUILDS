#!/bin/bash
#
# Welcome: the EndeavourOS greeter.
#
# Shows useful basic info about the EndeavourOS and its usage.
# Targeted for people new to EndeavourOS (but not new to Linux).
#
# TODO:
# - add icons to buttons once proper supported exists in yad

EOS_SCRIPTS_DIR=/usr/share/endeavouros/scripts

#################################################################################
EOS_SCRIPTS_YAD=$EOS_SCRIPTS_DIR/eos-script-lib-yad
test -r  $EOS_SCRIPTS_YAD || {
    echo "ERROR: cannot find $EOS_SCRIPTS_YAD" >&2
    exit 1
}
source $EOS_SCRIPTS_YAD
unset EOS_SCRIPTS_YAD

#export -f eos_yad
export -f eos_yad_terminal
export -f eos_yad_check_internet_connection
export -f eos_yad_GetArgVal
export -f eos_yad_RunCmdTermBash
export -f eos_yad_problem
export -f eos_yad_DIE
export -f eos_yad_WARN
export -f eos_yad__detectDE
export -f eos_yad_GetDesktopName
#################################################################################

export PROGNAME=eos-welcome
export PRETTY_PROGNAME="Welcome"
export PROG_PACKAGENAME="welcome"
export INSTALLER_PROGRAM=/usr/bin/calamares
export EOS_LIVEUSER=liveuser
export INSTALL_LOG_FILE=/home/$EOS_LIVEUSER/endeavour-install.log
# $INSTALL_LOG_FILE

eos_yad() {
    GDK_BACKEND=x11 /usr/bin/yad "$@"
}
export -f eos_yad

IsInstalling() {
    test $show_installer -eq 1 && return 0
    test -x $INSTALLER_PROGRAM && test "$LOGNAME" = "$EOS_LIVEUSER"
}

yad_calamares() {
    local mode="$1"                             # offline, online, choose
    local debug="$2"
    local prog=$INSTALLER_PROGRAM

    if [ ! -x $prog ] ; then
        eos_yad --text="$(basename $prog) is not available!" --title="Warning" --height=100 --width=200 --button=yad-quit:1
        return 1
    fi

    if [ "$mode" = "choose" ] ; then
        local t1="This is a community development release.\n"
        local t2="<b>Offline</b> method gives you an Xfce desktop with EndeavourOS theming.\n"
        local t3="<b>Online</b> method lets you choose your desktop, with vanilla theming.\n"
        local t4="\nDISCLAIMER: This release is a work-in-progress, please help us making it stable by reporting bugs."

        local answer=$( eos_yad --form --title="Choose installation method" --image=dialog-question \
                                --text="$t1$t2$t3$t4" \
                                --field="Method":CB 'Offline!Online' )
        test -n "$answer" || return 1
        mode=$(echo "$answer" | cut -d '|' -f 1 | tr [:upper:] [:lower:])
    fi
    case "$mode" in
        offline) ;;
        online)
            eos_yad_check_internet_connection verbose || return 1
            ;;
        *)  eos_yad_WARN "$PROGNAME: $FUNCNAME: unsupported mode '$mode'."
            return 1
            ;;
    esac

    local file=/usr/share/calamares/settings.conf_$mode
    if [ ! -r $file ] ; then
        eos_yad_WARN "$PROGNAME: $FUNCNAME: required file '$file' does not exist!"
        return 1
    fi

    sudo cp -praf /usr/share/calamares/settings.conf_$mode         /usr/share/calamares/settings.conf
    sudo cp -praf /usr/share/calamares/modules/packages.conf_$mode /usr/share/calamares/modules/packages.conf
    sudo cp -praf /usr/share/calamares/modules/welcome.conf_$mode  /usr/share/calamares/modules/welcome.conf

    cat <<EOF > $INSTALL_LOG_FILE
############## $INSTALL_LOG_FILE
############## Install started (UTC): $(date -u "+%Y-%m-%d %H:%M")

EOF
    if [ "$debug" = "yes" ] ; then
        touch $INSTALL_LOG_FILE
        local conf=/home/$EOS_LIVEUSER/.config/xfce4/terminal/terminalrc
        if [ -r $conf ] ; then
            echo "ScrollingLines=20000"    >> $conf
            echo "ScrollingUnlimited=TRUE" >> $conf
        fi
        eos_yad_RunCmdTermBash "tail -f $INSTALL_LOG_FILE" &
    fi

    bash -c "pkexec $prog -d >> $INSTALL_LOG_FILE"

    cat <<EOF >> $INSTALL_LOG_FILE

############## Install finished (UTC): $(date -u "+%Y-%m-%d %H:%M")
EOF
    # test $netlog -eq 1 && cat $INSTALL_LOG_FILE | curl -F 'f:1=<-' ix.io
}
export -f yad_calamares

yad_calamares_debug() {
    yad_calamares "$1" yes
}
export -f yad_calamares_debug

yad_UpdateMe() {
    local tmpfile=$(mktemp)
    cat <<EOF > $tmpfile
#!/bin/bash
pkexec pacman -Sy $PROG_PACKAGENAME --needed --noconfirm >& /dev/null
pkill -f "eos-welcome"
pkill -f "yad --plug"
pkill -f "yad --notebook"
eos-welcome &
rm -f $tmpfile
EOF
    bash $tmpfile
}
export -f yad_UpdateMe

yad_UpdateEOStoLatest() {    # Show what to update in order to set any EOS install at the "latest level".
    local removes=(kalu)
    local installs=(eos-update-notifier reflector-simple)
    local rem=() ins=()
    local xx
    for xx in "${removes[@]}" ; do
        pacman -Q "$xx" >& /dev/null && rem+=("$xx")
    done
    for xx in "${installs[@]}" ; do
        pacman -Q "$xx" >& /dev/null || ins+=("$xx")
    done
    
    local t1="To have your EndeavourOS install on par with the latest and greatest, the following actions are needed:\n"
    local t2="- uninstall packages: ${rem[*]}\n"
    local t3="- install packages: ${ins[*]}\n"
    local t="$t1"
    test -n "$rem" && t+="$t2"
    test -n "$ins" && t+="$t3"
    test "$t" = "$t1" && t+="Your system is already at the latest level!"
    eos_yad --text="$t" --title="Upgrading EndeavourOS level" --image=info --button=yad-ok:0
}
export -f yad_UpdateEOStoLatest

yad_PkgIssues() {
    local cpu
    local removes=()
    local title="Package issue detection"
    local intel=0 nvidia=0 amd=0
    local graphics
    local xx

    # having both amd and intel microcode packages are not needed and may cause issues with grub
    pacman -Q amd-ucode >& /dev/null && {
        pacman -Q intel-ucode >& /dev/null && {
            cpu=$(grep -w "^vendor_id" /proc/cpuinfo | head -n 1 | awk '{print $3}')
            case "$cpu" in
                GenuineIntel) removes+=(amd-ucode) ;;
                *)            removes+=(intel-ucode) ;;
            esac
        }
    }

    # detecting unnecessary video drivers
    graphics="$(device-info --vga ; device-info --display)"
    test -n "$(echo "$graphics" | grep "Intel Corporation")" && intel=1
    if [ -n "$(echo "$graphics" | grep "NVIDIA Corporation")" ] ; then
        nvidia=1
    elif [ -n "$(echo "$graphics" | grep "GeForce")" ] ; then
        nvidia=1
    fi
    if [ -n "$(echo "$graphics" | grep "Advanced Micro Devices")" ] ; then
        amd=1
    elif [ -n "$(echo "$graphics" | grep "AMD/ATI")" ] ; then
        amd=1
    elif [ -n "$(echo "$graphics" | grep "Radeon")" ] ; then
        amd=1
    fi
    if [ $intel -eq 0 ] ; then
        for xx in xf86-video-intel ; do
            pacman -Q $xx >& /dev/null && removes+=($xx)
        done
    fi
    if [ $nvidia -eq 0 ] ; then
        for xx in xf86-video-nouveau nvidia nvidia-dkms nvidia-390xx nvidia-390xx-dkms nvidia-lts nvidia-installer-dkms nvidia-settings ; do
            pacman -Q $xx >& /dev/null && removes+=($xx)
        done
    fi
    if [ $amd -eq 0 ] ; then
        for xx in xf86-video-amdgpu xf86-video-ati ; do
            pacman -Q $xx >& /dev/null && removes+=($xx)
        done
    fi

    # now remove potential unnecessary packages
    if [ -n "$removes" ] ; then
        local prompt="echo Package issues detected. Removing packages: ${removes[*]}"
        local cmd="su -c 'pacman -Rsn ${removes[*]} && grub-mkconfig -o /boot/grub/grub.cfg'"
        eos_yad_RunCmdTermBash "$prompt ; $cmd"
    else
        local t="No system package issues were detected."
        eos_yad --text="$t" --title="$title" --image=info --button=yad-ok:0
    fi
}
export -f yad_PkgIssues

yad_SystemUpdate() {
    local tmpfile=$(mktemp)
    cat <<EOF > $tmpfile
#!/bin/bash
_xx_main() {
  local updates="\$(checkupdates)"
  test -n "\$updates" && {
      echo "\$updates"
      yay -Syu # --noconfirm
  } || {
      echo "No updates."
  }
}
_xx_main
EOF
    chmod +x $tmpfile
    eos_yad_RunCmdTermBash "echo Checking for software updates... ; $tmpfile ; rm -f $tmpfile"
}
export -f yad_SystemUpdate

yad_InitializePacmanMirroring() {
    local tmpfile=$(mktemp)
    cat <<EOF > $tmpfile
#!/bin/bash
haveged -w 1024
pacman-key --init
pacman-key --populate
pacman-key --refresh-keys
pkill haveged
EOF
    chmod +x $tmpfile
    eos_yad_RunCmdTermBash "echo 'Initializing mirroring for pacman...' ; pkexec $tmpfile ; rm -f $tmpfile"
}
export -f yad_InitializePacmanMirroring

yad_Install() {
    # Install one or more given packages. Does not reinstall any packages.

    local yadcmd="eos_yad --text-info --title=Installer --wrap --tail --width=600 --height=500 --button=yad-quit:0"

    local pkg pkgs=()
    for pkg in "$@" ; do
        pacman -Q "$1" >& /dev/null || pkgs+=("$pkg")
    done
    test -z "$pkgs" && {
        echo "$*: already installed" | $yadcmd
        return
    }
    while true ; do
        echo "Installing ${pkgs[*]} ..."
        pkexec pacman -S --noconfirm "${pkgs[@]}"
        echo "Finished."
        break
    done |& $yadcmd
}
export -f yad_Install

yad_GetCurrentDM() {
    local current=$(ls -l /etc/systemd/system/display-manager.service | awk '{print $NF}')
    current="$(basename $current .service)"
    echo "$current"
}
export -f yad_GetCurrentDM

yad_ChangeDisplayManager() {
    local cmd count
    local dmlist="" dm
    local dms=(gdm lightdm lxdm sddm)
    local current=$(yad_GetCurrentDM)

    count="${#dms[@]}"

    cmd=(eos_yad --list --radiolist --title="Select Display Manager" --width=300 --height=200)
    cmd+=(--column="Selected":rd --column="DM name")

    for ((ix=0; ix<count; ix++)) ; do
        dm="${dms[$ix]}"
        case "$dm" in
            $current) cmd+=(true  "$dm") ;;
            *)        cmd+=(false "$dm") ;;
        esac
    done

    # selected new dm
    dm="$("${cmd[@]}" | cut -d '|' -f 2)"

    case "$dm" in
        "$current" | "") return ;;
    esac

    cmd=""
    pacman -Q "$dm" >& /dev/null || {
        case "$dm" in
            lightdm) cmd+="pacman -S ${dm}{,-gtk-greeter{,-settings}} --noconfirm >& /dev/null && ";;
            *)       cmd+="pacman -S $dm --noconfirm >& /dev/null && " ;;
        esac
    }
    cmd+="systemctl disable $current && systemctl enable $dm"
    pkexec bash -c "$cmd"

    if [ "$(yad_GetCurrentDM)" = "$dm" ] ; then
        echo "Reboot is required for the changes to take effect." | \
            eos_yad --text-info --title="DM changed to $dm" --wrap --width=300 --height=200 --button=yad-quit:0
    else
        echo "Changing DM failed." | \
            eos_yad --text-info --title="Warning" --width=300 --height=200 --button=yad-quit:0
    fi
}
export -f yad_ChangeDisplayManager

INSTALL() {
  local handle="$1"
  local tabnum="$2"
  local _exclamation='&#33;'   # '!'

  eos_yad --plug="$handle" --tabnum="$tabnum" \
          --form \
          --columns=2 \
          --image=gtk-save \
          --text="<b>Installing EndeavourOS to disk</b>" --text-align=left \
          --field="<b>Start the Installer</b>!!Start the EndeavourOS installer along with a debug terminal":fbtn \
                                                                                            'bash -c "yad_calamares_debug choose"' \
          --field="Update this app$_exclamation!!Update this app and restart it":fbtn       'bash -c yad_UpdateMe' \
          --field="Initialize pacman keys!!Initialize pacman keys":fbtn                     'bash -c yad_InitializePacmanMirroring' \
          --field="Partition manager!!Gparted allows examining and managing disk partitions":fbtn    'gparted' \
          --field="Latest release info!!Info about the latest release":fbtn      "$_BROWSER https://endeavouros.com/latest-release" \
          --field="Installation tips!!Installation tips":fbtn                    "$_BROWSER https://endeavouros.com/docs/installation" \
          --field="Troubleshoot!!System Rescue":fbtn                             "$_BROWSER https://endeavouros.com/docs/system-rescue" \
      &> /dev/null &

#          --field="<b>Start the Installer (no terminal)$_exclamation</b>!!Start the EndeavourOS installer":fbtn  'bash -c "yad_calamares choose"' \

  # use 'gparted' as the icon for gparted!
}

GeneralInfo() {
  local handle="$1"
  local tabnum="$2"
  eos_yad --plug="$handle" --tabnum="$tabnum" \
          --form \
          --columns=3 \
          --image=dialog-question \
          --text="<b>Find your way at the EndeavourOS website!</b>" --text-align=left \
          --field="Web site!!https://endeavouros.com":fbtn                        "$_BROWSER https://endeavouros.com" \
          --field="Wiki!!Featured articles":fbtn                                  "$_BROWSER https://endeavouros.com/wiki" \
          --field="News!!News and articles":fbtn                                  "$_BROWSER https://endeavouros.com/posts" \
          --field="Forum!!Ask, comment, and chat in our friendly forum!":fbtn     "$_BROWSER https://forum.endeavouros.com" \
          --field="Donate!!Help us keep EndeavourOS running":fbtn                 "$_BROWSER https://endeavouros.com/donate" \
          --field="About $PRETTY_PROGNAME!!More info about this app":fbtn         "$_BROWSER https://endeavouros.com/docs/applications/welcome-greeter" \
      &> /dev/null &
}

AfterInstall() {
  local handle="$1"
  local tabnum="$2"

  local _exclamation='&#33;'   # '!'
  local _and='&#38;'           # '&'

  eos_yad --plug="$handle" --tabnum="$tabnum" \
          --form \
          --columns=2 \
          --image=dialog-information \
          --text="<b>After install tasks</b>" --text-align=left \
          --field=" Update Mirrors!applications-internet!Update list of mirrors before system update":fbtn             'reflector-simple' \
          --field=" Update System!system-software-update!Update System Software":fbtn                                  'bash -c yad_SystemUpdate' \
          --field=" Detect package issues!face-glasses!Detect any potential issues on system packages":fbtn            'bash -c yad_PkgIssues' \
          --field=' EndeavourOS to latest?!system-software-install!Show what to do to get your system to the latest EndeavourOS "level"':fbtn \
                                                                                                                       'bash -c yad_UpdateEOStoLatest' \
          --field=" Change Display Manager!preferences-desktop-display!Use a different display manager":fbtn           'bash -c yad_ChangeDisplayManager' \
          --field=" EndeavourOS wallpaper!preferences-desktop-wallpaper!Change desktop wallpaper to EOS default":fbtn  'eos-set-background-picture' \
          --field="Package management!!How to manage packages with 'pacman'":fbtn      "$_BROWSER https://endeavouros.com/docs/pacman" \
          --field="AUR $_and yay$_exclamation!!Arch User Repository info":fbtn         "$_BROWSER https://endeavouros.com/docs/aur/yay" \
          --field="Hardware and Network!!Get your hardware working":fbtn               "$_BROWSER https://endeavouros.com/docs/hardware-and-network" \
          --field="Bluetooth!!Bluetooth advice":fbtn                                   "$_BROWSER https://wiki.archlinux.org/index.php/Bluetooth" \
          --field="NVIDIA users$_exclamation!!Use NVIDIA installer":fbtn               "$_BROWSER https://endeavouros.com/docs/hardware-and-network/nvidia-installer" \
          --field="Forum tips!!Help us help you!":fbtn                                 "$_BROWSER https://endeavouros.com/docs/forum/how-to-include-systemlogs-in-your-post" \
      &> /dev/null &
}

AddMoreApps() {
  local handle="$1"
  local tabnum="$2"

  local gufw=""            #=gufw
  local libreoffice=""     #=libreoffice
  local chromium=""        #=chromium-browser
  local bluetooth=""       #=bluetooth

  eos_yad --plug="$handle" --tabnum="$tabnum" \
          --form \
          --columns=3 \
          --image=system-software-install \
          --text="<b>Install popular apps</b>" --text-align=left \
          --field=" LibreOffice!$libreoffice!Office tools (libreoffice-fresh)":fbtn  'bash -c "yad_Install libreoffice-fresh"' \
          --field=" Chromium Web Browser!$chromium!Web Browser":fbtn                 'bash -c "yad_Install chromium"' \
          --field=" Firewall!$gufw!Gufw firewall":fbtn                               'bash -c "yad_Install gufw"' \
          --field=" Bluetooth Manager for Xfce!$bluetooth!Blueberry":fbtn            'bash -c "yad_Install blueberry bluez-utils"' \
      &> /dev/null &
}

About() {
  local handle="$1"
  local tabnum="$2"

  Usage | eos_yad --plug="$handle" --tabnum="$tabnum" \
                  --text-info \
                  --text="<b>More info about the $PRETTY_PROGNAME app</b>" \
                  --text-align=left \
                  --image=help-about \
      &> /dev/null &
}


# Fields explanation:
# --field="ButtonName!IconName!Tooltip":fbtn "Command"

### These variables are required:

_NOTEBOOK_TITLE="$PRETTY_PROGNAME v$(pacman -Q $PROG_PACKAGENAME | awk '{print $2}' | cut -d '-' -f 1)"        # main window title

CreateNotebookCommands() {
    IsInstalling && {
        _NOTEBOOK_TABS=(                             # names of functions above
            INSTALL
            GeneralInfo
        )
    } || {
        _NOTEBOOK_TABS=(                             # names of functions above
            GeneralInfo
            AfterInstall
            AddMoreApps
            #About
        )
    }
}

####################### DO NOT CHANGE ANYTHING AFTER THIS LINE! ######################################################

DIE() {
    local title="Error"
    while true ; do
        echo "Error: $1."
        Usage
        break
    done | eos_yad_problem "$title" "$@"
    exit 1
}

WelcomeHelp() {
    Usage | eos_yad --text-info --title="$PRETTY_PROGNAME Help" --width=600 --height=500 \
                    --text="<b>More info about the $PRETTY_PROGNAME app</b>" \
                    --text-align=left \
                    --image=help-about --button=yad-ok:0
}
export -f WelcomeHelp

SetBrowser() {
    local xx
    for xx in xdg-open exo-open firefox chromium ; do  # use one of these browser commands
        if [ -x /usr/bin/$xx ] ; then
            _BROWSER=/usr/bin/$xx        # for showing external links
            return
        fi
    done
    DIE "$FUNCNAME: cannot find a browser"
}

PrepareTabs() {
    local handle="$1"
    local xx ix
    ix=1
    for xx in "${_NOTEBOOK_TABS[@]}" ; do
        $xx "$handle" "$((ix++))"
    done
}

SeparateWordsWithSpaces() { # add a space before a capital letter inside a word
    local tabname="$1"

    if [ "$(echo "$tabname" | tr -d '[a-z]')" = "$tabname" ] ; then
        echo "$tabname"           # all capital letters ==> don't change
    else
        echo "$tabname" | sed -e 's|\([A-Z]\)| \1|g' -e 's|^ ||'  # add space before capital letters
    fi
}

KillExtraYad() {
    # There may be an extra "yad" process in vain because
    # the "save session" feature of Xfce has not stored the proper command for this app.
    #
    # The extra yad process will be eliminated by
    #   - removing the extra "yad" command from the stored session
    #   - killing the extra "yad" process if it exists

    # Remove the extra "yad" command from the saved session.
    local savefile="$(ls -1 "$HOME"/.cache/sessions/xfce4-session-* 2>/dev/null | grep -vP "\.bak$" | tail -n 1)"
    if [ -n "$savefile" ] ; then
        local count=$(grep ^LegacyCount= "$savefile" | cut -d '=' -f 2)
        local endcount=$count
        if [ "$(grep "^Legacy0_Command=yad$" "$savefile")" != "" ] || [ "$(grep "^Legacy0_Command=eos_yad$" "$savefile")" != "" ] ; then
            ((endcount--))
            sed -i "$savefile" \
                -e 's|^Legacy0_Screen=.*$||' \
                -e 's|^Legacy0_Command=.*$||' \
                -e 's|^Legacy0_ClientMachine=.*$||'
        fi
        if [ $endcount -ne $count ] ; then
            sed -i "$savefile" -e 's|^LegacyCount=.*$|LegacyCount='$endcount'|'
        fi
    fi

    # Now check if an extra "yad" process is already running. If so, simply kill it.

    sleep 0.1    # must wait a bit!

    local yadlines yadline
    local pid ppid lastword
    local processes="$(ps -ef | grep -w yad | grep -v "grep -w yad" | grep -v "^root")"  # search 'yad' without 'root'
    local problem_ppid=1

    # search for the right "extra" yad process
    if [ "$(echo "$processes" | awk '{print $NF}')" = "yad" ] ; then
        readarray -t yadlines <<< "$(echo "$processes")"
        for yadline in "${yadlines[@]}" ; do
            lastword="$(echo "$yadline" | awk '{print $NF}')"
            ppid="$(echo "$yadline" | awk '{print $3}')"
            if [ "$lastword" = "yad" ] && [ "$ppid" = "$problem_ppid" ] ; then
                # found the "extra" yad process
                pid="$(echo "$yadline" | awk '{print $2}')"
                kill $pid
                echo "$yadline" > /tmp/save-session-zombie-yad-killed.log
            fi
        done
    fi
}


WelcomeSettings() {
    local value="$1"   # enable, disable, check
    declare -A defaults
    local xx

    defaults[Greeter]="Greeter=enable"
    defaults[LastCheck]="LastCheck=0"
    defaults[OnceDaily]="OnceDaily=no"

    # Make sure we have sensible initial values in the config file.
    if [ ! -r "$WELCOME_CONFIG" ] ; then
        cat <<EOF > "$WELCOME_CONFIG"
## Configuration file for $PROGNAME.
# Note: using bash syntax.
#
# 'Greeter'   values: enable or disable.
# 'OnceDaily' values: no or yes; yes means $PROGNAME is shown only once a day.
# 'LastCheck' values: automatically filled by $PROGNAME.

EOF
    fi

    for xx in Greeter OnceDaily LastCheck ; do
        if [ -z "$(grep "^${xx}=" "$WELCOME_CONFIG" 2>/dev/null)" ] ; then
            echo "${defaults[$xx]}" >> "$WELCOME_CONFIG"
        fi
    done

    # Now the initial values are in order.

    case "$value" in
        enable | disable)
            sed -i "$WELCOME_CONFIG" -e 's|^Greeter=.*$|Greeter='"$value"'|'
            ;;
    esac

    case "$value" in
        disable)
            echo "To run $PRETTY_PROGNAME again, start a terminal and run '$PROGNAME --enable'." | \
                eos_yad --text-info --image=dialog-information --text="Re-enabling $PRETTY_PROGNAME:" \
                        --title="How to re-enable $PRETTY_PROGNAME" \
                        --geometry=500x200 --wrap \
                        --fontname="Monospace Regular 12" \
                        --button=" I remember!face-angel!I promise":0 >& /dev/null
            ;;
        check)
            grep "^Greeter=" "$WELCOME_CONFIG" 2>/dev/null | cut -d '=' -f 2
            ;;
        continue)
            # IsInstalling && { echo yes ; return ; }  # not needed, installer has default values
            if [ "$(grep "^OnceDaily=" "$WELCOME_CONFIG" 2>/dev/null | cut -d '=' -f 2)" = "no" ] ; then
                echo yes
                return
            fi
            local date="$(date +%Y%m%d)"
            if [ "$(grep "^LastCheck=" "$WELCOME_CONFIG" 2>/dev/null | cut -d '=' -f 2)" = "$date" ] ; then
                echo no
            else
                echo yes
            fi
            sed -i "$WELCOME_CONFIG" -e 's|^LastCheck=.*$|LastCheck='"$date"'|'
            ;;
    esac
}

UpdateInstallScripts() {
    IsInstalling || return

    local URL=https://github.com/endeavouros-team/install-scripts/raw/master
    local scripts=(                        # may need to adjust files ...
        calamares_switcher
        chrooted_cleaner_script.sh
        cleaner_script.sh
        pacstrap_calamares
        update-mirrorlist
    )
    local script
    local tmpdir=$(mktemp -d)

    for script in "${scripts[@]}"; do
        wget -q --timeout=10 -O "$tmpdir/$script" "$URL/$script" || DIE "cannot fetch script '$script'."
        chmod +x "$tmpdir/$script"
    done
    for script in "${scripts[@]}"; do
        sudo rm -f /usr/bin/"$script"
        sudo mv "$tmpdir/$script" /usr/bin
    done
    rm -rf $tmpdir
}

Usage() {
    cat <<EOF
Usage: $PROGNAME [options]

Options:
--startdelay=X    Wait before actually starting this app.
                  X value syntax is the same as in 'sleep'.
--enable | -f     Enable this $PRETTY_PROGNAME app.
--disable         Disable this $PRETTY_PROGNAME app.
--version         Show the version of this app.

To have $PRETTY_PROGNAME app started when you log in, make sure
- $PRETTY_PROGNAME app is selected in the Autostart feature of the DE
    OR
- "Hidden=false" is set in file /etc/xdg/autostart/welcome.desktop

On DEs that do not work well with Autostart:
You may also disable the $PRETTY_PROGNAME app from the app itself
- with the --disable option
- with a button in the app (some DEs only)

To re-enable the app, use the terminal command
    $PROGNAME --enable

Note: check also settings in the configuration file $WELCOME_CONFIG.
EOF
}
export -f Usage

StartHere() {
    local WELCOME_CONFIG="$HOME/.config/EOS-greeter.conf"     # "Welcome" used to be "Greeter" ...
    KillExtraYad

    local show_installer=0    # explicitly show the installer, for testing only
    local start_delay=0
    local is_installing
    #local netlog=0

    local arg

    for arg in "$@" ; do
        case "$arg" in
            --enable|-f) WelcomeSettings enable ;;
            --disable)   WelcomeSettings disable ; return ;;
            --startdelay=*) start_delay="${arg:13}" ;;
            --installer) show_installer=1 ;;
            --version) pacman -Q $PROG_PACKAGENAME | awk '{print $2}' ; return ;;
            #--netlog) netlog=1 ;;
            -*) DIE "unsupported option '$arg'" ;;
            *)  DIE "unsupported parameter '$arg'" ;;
        esac
    done

    if [ "$(WelcomeSettings continue)" = "no" ] ; then
        return
    fi

    CreateNotebookCommands

    test "$(WelcomeSettings check)" != "enable" && {
        echo "$PRETTY_PROGNAME app is disabled. To start it anyway, use option --enable."
        return
    }

    IsInstalling
    is_installing=$?

    if [ $is_installing -eq 0 ] ; then
        :
        #if [ -z "$(grep '=/usr/share/endeavouros/scripts/welcome' /home/$EOS_LIVEUSER/.config/xfce4/panel/launcher-*/*.desktop)" ] ; then
        #    xfce4-panel --add=launcher $EOS_SCRIPTS_DIR/welcome-panel.desktop
        #fi
    else
        eos_yad_check_internet_connection verbose 10 || return 1
    fi

    # UpdateInstallScripts

    if [ "$start_delay" != "0" ] ; then
        sleep "$start_delay"
    fi

    SetBrowser

    local handle="$(shuf -i 700000-999999 -n 1)"
    local tab tabname
    local notebook   # contains the main yad command

    PrepareTabs "$handle" || DIE "PrepareTabs failed"

    # Create the yad command gradually into an array 'notebook':

    notebook=(eos_yad --notebook --key="$handle" --center --title="$_NOTEBOOK_TITLE")
    for tab in "${_NOTEBOOK_TABS[@]}" ; do
        tabname="$(SeparateWordsWithSpaces "$tab")"
        notebook+=(--tab="$tabname")
    done

    IsInstalling || notebook+=(--active-tab=2)

    notebook+=(--button=" Help!help-contents!":"bash -c WelcomeHelp")

    case "$(eos_yad_GetDesktopName)" in
        KDE | GNOME | GNOME3)
            notebook+=(--button=" See you later!face-cool!Keep $PRETTY_PROGNAME enabled":0)
            notebook+=(--button=" Don't show me anymore!face-crying!Disable $PRETTY_PROGNAME":5)
            "${notebook[@]}"
            local result=$?
            case "$result" in
                5) WelcomeSettings disable ;;
                0|252) WelcomeSettings enable ;;
            esac
            ;;
        *)
            notebook+=(--button=" See you later!face-cool!":0)
            "${notebook[@]}"
            ;;
    esac
}

StartHere "$@"

exit 0
